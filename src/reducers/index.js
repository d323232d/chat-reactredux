import { combineReducers } from "redux";
import messageReduser from "../components/Content/messages.reducer";

const rootReducer = combineReducers({
  chat: messageReduser,
});

export default rootReducer;