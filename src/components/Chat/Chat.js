import React from 'react';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import { Loading } from '../Loader/Loader';
import Content from '../Content/Content';
import './Chat.css';
import { connect } from 'react-redux';
import * as actions from '../Content/actions';
import Modal from '../Content/Modal/Modal';


const fetchData = async (url) => {
  const data = await fetch(url)
    .then(response => response.json())
  return data;
};

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.url = props.url
  }

  componentDidMount() {
    fetchData(this.url)
      .then(data => {
        this.props.setMessages(data);
        this.props.setPreloader(false);
      });
  }

  render() {
    return (
      <>
        <Header />
        <main className="chat">
          {this.props.preloader
            ? <Loading />
            : <Content />}
        </main>
        <Footer />
        {this.props.editModal ? <Modal /> : ''}
      </>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    preloader: state.chat.preloader,
    editModal: state.chat.editModal
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);