import {
  ADD_MESSAGE,
  DEL_MESSAGE,
  SET_MASSAGES,
  SET_PRELOADER,
  SHOW_MODAL,
  SAVE_CHANGE,
  CANCEL_CHANGE
} from './actions.type';

export const setMessages = (messagesList) => ({
  type: SET_MASSAGES,
  payload: messagesList
});

export const setPreloader = (bool) => ({
  type: SET_PRELOADER,
  payload: bool
});

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: message
});

export const delMessage = (id) => ({
  type: DEL_MESSAGE,
  payload: id
});

export const showModal = (id, text) => ({
  type: SHOW_MODAL,
  // payload: true,
  payload: {
    isOpen: true,
    editMessage: {
      id,
      text,
    }
  }
});

export const cancelChange = () => ({
  type: CANCEL_CHANGE,
  payload: false
});
export const saveChange = (newList) => ({
  type: SAVE_CHANGE,
  payload: newList
});



