import React from 'react';
import moment from "moment";

const Message = (props) => {
  let message = props.message;

  const likeMessage = e => {
    e.preventDefault();
    const target = e.target;
    target.parentElement.classList.toggle('message-liked');
    target.parentElement.classList.toggle('message-like');
  };

  return (
    <li id={message.id} className="message collection-item avatar" data-userid={message.userId}>
      <img src={message.avatar} alt="" className="message-user-avatar circle" />
      <div className="message-intro">
        <div className="message-user-name">{message.user}</div>
        <div className="message-text">{message.text}</div>
        <div className="message-time">{moment(message.createdAt).format("HH:mm")}</div>
      </div>
      <a href="/" className="message-like"><i className="material-icons" onClick={ e => likeMessage(e)}>favorite</i></a>
    </li>    
  )
};

export default Message;