import React from 'react';
import moment from "moment";

import { connect } from 'react-redux';
import * as actions from '../actions.js';

const UserMessage = (props) => {
  let message = props.message;

  const onHendleDelete = (e, id) => {
    e.preventDefault();
    const target = e.target;
    if (target.closest('.message')) {
      props.delMessage(id);
    }
  };

  const onHendleEdit = (e) => {
    e.preventDefault();
    props.showModal(message.id, message.text);
  }

  return (
    <li id={message.id} className="message right collection-item own-message" data-userid={message.userId}>
      <div className="message-intro">
        <div className="message-text">{message.text}</div>
        <div className="message-time">{moment(message.createdAt).format("HH:mm")}</div>
      </div>
      <a href="/" className="message-edit">
        <i className="material-icons"
          onClick={e => onHendleEdit(e)}
        >edit</i>
      </a>
      <a href="/" className="message-delete">
        <i
          className="material-icons"
          onClick={e => onHendleDelete(e, message.id)}
        >clear</i>
      </a>
    </li>
  )
};

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages
  }
};

const mapDispatchToProps = {
  ...actions
};

export default connect(mapStateToProps, mapDispatchToProps)(UserMessage);