import {
  ADD_MESSAGE,
  DEL_MESSAGE,
  SAVE_CHANGE,
  SET_MASSAGES,
  SET_PRELOADER,
  SHOW_MODAL,
  CANCEL_CHANGE,
} from './actions.type';

const initialState = {
  messages: [],
  editModal: false,
  preloader: true,
  editMessage: null
};

// reducer
export default function messageReduser(state = initialState, action) {
  switch (action.type) {
    case SET_MASSAGES:
      return {
        ...state,
        messages: [...action.payload],
        preloader: false
      };
    case SET_PRELOADER:
      return {
        ...state,
        preloader: action.payload
      };
    case ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.payload]
      };
    case DEL_MESSAGE:
      const id = action.payload;
      const updatedMessagesList = state.messages.filter(item => item.id !== id);
      return {
        ...state,
        messages: [...updatedMessagesList]
      };
    case SHOW_MODAL:
      return {
        ...state,
        editModal: action.payload.isOpen,
        editMessage: { ...action.payload.editMessage }
      };
    case CANCEL_CHANGE:
      return {
        ...state,
        editModal: action.payload
      };
    case SAVE_CHANGE:
      //      
      return {
        ...state,
        messages: [...action.payload],
        editModal: false,
        editMessage: null
      };

    default:
      return state;
  }
}
